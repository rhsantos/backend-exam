## Docker - Alternative 1
With docker and docker-compose already installed

- Create container
`$ sudo docker-compose up --build -d`

- Apply migrations
`$ ./manage.py migrate`

- Run tests
`$ docker-compose run api python manage.py test`

- Run pep8
`$ docker-compose run api pycodestyle  --exclude='*/migrations/**' --max-line-length=100 .`*

- Enter the container
`$ sudo docker-compose up exec api bash`

- Run server
`$ ./manage.py runserver 0.0.0.0:8000`


## Virtual-Env - Alternative 2
Steps virtual env:

- Create virtual env
`$ python3.6 -m venv env`

- Activate venv
`$ source env/bin/active`

- Install dependencies
`$ pip install -r requirements.txt`

- Apply migrations
`$ ./manage.py migrate`

- Run tests
`$ python manage.py test`

- Run pep8
`$ pycodestyle  --exclude='*/migrations/**' --max-line-length=100 .`*

- Run server
`$ ./manage.py runserver 0.0.0.0:8000`


\* Migrations have been exclude because are is not in my control and max-line-lengh change
because a row in django settings is higger than default value