from rest_framework.viewsets import ModelViewSet
from tasks.models import Task
from .serializer import TaskSerializer


class TaskViewSet(ModelViewSet):

    queryset = Task.objects.all().order_by('-id')

    serializer_class = TaskSerializer
