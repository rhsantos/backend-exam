from rest_framework.routers import SimpleRouter
from .viewset import TaskViewSet


router = SimpleRouter(trailing_slash=False)
router.register(r'tasks', TaskViewSet)
