from django.db import models
from users.models import User


class Task(models.Model):
    user_id = models.ForeignKey(
        User,
        related_name='tasks',
        related_query_name='tasks',
        on_delete=models.CASCADE,
        verbose_name='User ID',
    )

    description = models.CharField(
        max_length=50,
        verbose_name='Description',
        help_text=('Description task')
    )
