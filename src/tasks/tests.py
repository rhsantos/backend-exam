from rest_framework import status
from django.urls import reverse
from rest_framework.test import (
    APITestCase,
    APIRequestFactory
)
from users.models import User
from .models import Task


class CreateTaskTests(APITestCase):
    def setUp(self):
        self.url = '/tasks'
        self.user = User.objects.create(
            name='Ana',
            email='ana@email.com',
            age=18,
            user_type='admin'
        )

        self.task = {
            'user_id': self.user.id,
            'description': 'Description'
        }

    def test_task_creation(self):
        response = self.client.post(self.url, self.task, format='json')

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(response.data['description'], 'Description')

    def test_task_creation_with_description_bigger_than_50_characters(self):
        self.task['description'] = '''namenamenamenamenamenamenamename
                                      namenamenamenamenamenamenamename'''

        response = self.client.post(self.url, self.task, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(len(response.data.keys()), 1)
        self.assertTrue('description' in response.data)

    def test_task_creation_with_user_not_found(self):
        self.task['user_id'] = 5

        response = self.client.post(self.url, self.task, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(len(response.data.keys()), 1)
        self.assertTrue('user_id' in response.data)


class UpdateTaskTests(APITestCase):
    def setUp(self):
        self.user = User.objects.create(
            name='Ana',
            email='ana@email.com',
            age=18,
            user_type='admin'
        )

        self.task = Task.objects.create(
            user_id=self.user,
            description='Description'
        )

        self.url = '/tasks/{}'.format(self.task.id)

        self.data = {
            'user_id': self.user.id,
            'description': 'Description Update'
        }

    def test_task_update(self):
        response = self.client.put(self.url, self.data, format='json')

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['description'], 'Description Update')

    def test_task_update_with_description_bigger_than_50_characters(self):
        self.data['description'] = '''namenamenamenamenamenamenamename
                                      namenamenamenamenamenamenamename'''

        response = self.client.put(self.url, self.data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(len(response.data.keys()), 1)
        self.assertTrue('description' in response.data)

    def test_task_update_with_user_not_found(self):
        self.data['user_id'] = 5

        response = self.client.put(self.url, self.data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(len(response.data.keys()), 1)
        self.assertTrue('user_id' in response.data)


class DeleteTaskTests(APITestCase):
    def setUp(self):
        self.user = User.objects.create(
            name='Ana',
            email='ana@email.com',
            age=18,
            user_type='admin'
        )

        self.task = Task.objects.create(
            user_id=self.user,
            description='Description'
        )

        self.url = '/tasks/{}'.format(self.task.id)

    def test_task_delete(self):
        response = self.client.delete(self.url, format='json')

        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_task_delete_not_found(self):
        url = '/tasks/{}'.format(5)
        response = self.client.delete(url, format='json')

        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)


class GetTaskTests(APITestCase):
    def setUp(self):
        self.user = User.objects.create(
            name='Ana',
            email='ana@email.com',
            age=18,
            user_type='admin'
        )

        self.task1 = Task.objects.create(
            user_id=self.user,
            description='Description 1'
        )

        self.task1 = Task.objects.create(
            user_id=self.user,
            description='Description 1'
        )

        self.url = '/tasks'

    def test_task_list(self):
        response = self.client.get(self.url, format='json')

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 2)

    def test_task_retrieve(self):
        url = '/tasks/{}'.format(self.task1.id)
        response = self.client.get(url, format='json')

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['description'], 'Description 1')
        self.assertEqual(response.data['user_id'], self.user.id)

    def test_task_retrieve_not_found(self):
        url = '/tasks/{}'.format(5)
        response = self.client.get(url, format='json')

        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
