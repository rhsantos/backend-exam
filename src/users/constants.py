MIN_AGE = 18

ADMIN = 'admin'
MANAGER = 'manager'
DEFAULT = 'default'
TYPES = (
    (ADMIN, ADMIN),
    (MANAGER, MANAGER),
    (DEFAULT, DEFAULT)
)
