# Generated by Django 2.2 on 2019-04-18 01:03

import django.core.validators
from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='User',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(help_text='User name', max_length=50, verbose_name='Name')),
                ('email', models.EmailField(help_text='User email', max_length=200, verbose_name='Email')),
                ('age', models.PositiveIntegerField(help_text='Age of the user', validators=[django.core.validators.MinValueValidator(18)], verbose_name='Age')),
                ('user_type', models.CharField(choices=[('admin', 'admin'), ('manager', 'manager'), ('default', 'default')], help_text='User type', max_length=200, verbose_name='Type')),
            ],
        ),
    ]
