from rest_framework.routers import SimpleRouter
from .viewset import UserViewSet


router = SimpleRouter(trailing_slash=False)
router.register(r'users', UserViewSet)
