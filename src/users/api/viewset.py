from rest_framework.viewsets import ModelViewSet
from rest_framework.response import Response
from rest_framework.decorators import detail_route
from rest_framework import status
from django.http import Http404

from users.models import User
from .serializer import UserSerializer
from tasks.api.serializer import TaskSerializer


class UserViewSet(ModelViewSet):

    queryset = User.objects.all().order_by('-id')

    serializer_class = UserSerializer

    @detail_route(methods=['get'])
    def tasks(self, request, pk):
        try:
            user = User.objects.get(id=pk)
            tasks = TaskSerializer(user.tasks.all(), many=True).data
            return Response(tasks, status=status.HTTP_200_OK)
        except Exception:
            raise Http404()
