from rest_framework import status
from django.urls import reverse
from rest_framework.test import (
    APITestCase,
    APIRequestFactory
)
from .models import User
from tasks.models import Task


class CreateUserTests(APITestCase):
    def setUp(self):
        self.url = '/users'
        self.user = {
            'name': 'Ana',
            'email': 'ana@email.com',
            'age': 18,
            'user_type': 'admin'
        }

    def test_user_creation(self):
        response = self.client.post(self.url, self.user, format='json')

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(response.data['name'], 'Ana')

    def test_user_creation_with_age_less_than_18(self):
        self.user['age'] = 17

        response = self.client.post(self.url, self.user, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(len(response.data.keys()), 1)
        self.assertTrue('age' in response.data)

    def test_user_creation_with_email_is_not_in_format(self):
        self.user['email'] = 'p1@'

        response = self.client.post(self.url, self.user, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(len(response.data.keys()), 1)
        self.assertTrue('email' in response.data)

    def test_user_creation_with_name_bigger_than_50_characters(self):
        self.user['name'] = '''namenamenamenamenamenamenamename
                               namenamenamenamenamenamenamename'''

        response = self.client.post(self.url, self.user, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(len(response.data.keys()), 1)
        self.assertTrue('name' in response.data)

    def test_user_creation_with_user_type_out_of_choices(self):
        self.user['user_type'] = 'none'

        response = self.client.post(self.url, self.user, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(len(response.data.keys()), 1)
        self.assertTrue('user_type' in response.data)


class UpdateUserTests(APITestCase):
    def setUp(self):
        self.user = User.objects.create(
            name='Ana',
            email='ana@email.com',
            age=18,
            user_type='admin'
        )

        self.url = '/users/{}'.format(self.user.id)

        self.data = {
            'name': 'Maria',
            'email': 'maria@email.com',
            'age': 29,
            'user_type': 'default',
        }

    def test_user_update(self):
        response = self.client.put(self.url, self.data, format='json')

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['name'], 'Maria')
        self.assertEqual(response.data['email'], 'maria@email.com')
        self.assertEqual(response.data['age'], 29)
        self.assertEqual(response.data['user_type'], 'default')

    def test_user_update_with_age_less_than_18(self):
        self.data['age'] = 17

        response = self.client.put(self.url, self.data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(len(response.data.keys()), 1)
        self.assertTrue('age' in response.data)

    def test_user_update_with_email_is_not_in_format(self):
        self.data['email'] = 'p1@'

        response = self.client.put(self.url, self.data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(len(response.data.keys()), 1)
        self.assertTrue('email' in response.data)

    def test_user_update_with_name_bigger_than_50_characters(self):
        self.data['name'] = '''namenamenamenamenamenamenamename
                               namenamenamenamenamenamenamename'''

        response = self.client.put(self.url, self.data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(len(response.data.keys()), 1)
        self.assertTrue('name' in response.data)

    def test_user_update_with_user_type_out_of_choices(self):
        self.data['user_type'] = 'none'

        response = self.client.put(self.url, self.data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(len(response.data.keys()), 1)
        self.assertTrue('user_type' in response.data)


class DeleteUserTests(APITestCase):
    def setUp(self):
        self.user = User.objects.create(
            name='Ana',
            email='ana@email.com',
            age=18,
            user_type='admin'
        )

        self.url = '/users/{}'.format(self.user.id)

    def test_user_delete(self):
        response = self.client.delete(self.url, format='json')

        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_user_delete_not_found(self):
        url = '/users/{}'.format(5)
        response = self.client.delete(url, format='json')

        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)


class GetUserTests(APITestCase):
    def setUp(self):
        self.user1 = User.objects.create(
            name='Ana',
            email='ana@email.com',
            age=18,
            user_type='admin'
        )

        self.user2 = User.objects.create(
            name='Maria',
            email='maria@email.com',
            age=50,
            user_type='default'
        )

        self.url = '/users'

    def test_user_list(self):
        response = self.client.get(self.url, format='json')

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 2)

    def test_user_retrieve(self):
        url = '/users/{}'.format(self.user1.id)
        response = self.client.get(url, format='json')

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['name'], 'Ana')
        self.assertEqual(response.data['email'], 'ana@email.com')
        self.assertEqual(response.data['age'], 18)
        self.assertEqual(response.data['user_type'], 'admin')

    def test_user_retrieve_not_found(self):
        url = '/users/{}'.format(5)
        response = self.client.get(url, format='json')

        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)


class GetTasksUserTests(APITestCase):
    def setUp(self):
        self.user = User.objects.create(
            name='Ana',
            email='ana@email.com',
            age=18,
            user_type='admin'
        )

        self.task1 = Task.objects.create(
            user_id=self.user,
            description='Description 1'
        )

        self.task2 = Task.objects.create(
            user_id=self.user,
            description='Description 2'
        )

        self.url = '/users/{}/tasks'.format(self.user.id)

    def test_task_list(self):
        response = self.client.get(self.url, format='json')

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 2)

    def test_user_retrieve_not_found(self):
        url = '/users/{}/tasks'.format(5)
        response = self.client.get(url, format='json')

        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
