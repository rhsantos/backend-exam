from django.db import models
from django.core.validators import MinValueValidator
from .constants import (
    MIN_AGE,
    TYPES
)


class User(models.Model):
    name = models.CharField(
        max_length=50,
        verbose_name='Name',
        help_text=('User name')
    )

    email = models.EmailField(
        max_length=200,
        verbose_name='Email',
        help_text=('User email')
    )

    age = models.PositiveIntegerField(
        validators=[MinValueValidator(MIN_AGE)],
        verbose_name='Age',
        help_text=('Age of the user')
    )

    user_type = models.CharField(
        max_length=200,
        choices=TYPES,
        verbose_name='Type',
        help_text=('User type')
    )

    def __str__(self):
        return str(self.id)
